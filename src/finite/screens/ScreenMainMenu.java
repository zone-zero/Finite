package finite.screens;

import composition.animation.Animation;
import composition.graphics.GraphicsCore;
import composition.input.KeyboardControls;
import composition.localization.Locale;
import composition.media.AudioPlayer;
import composition.screen.Screen;
import composition.screen.ScreenStarter;
import composition.stats.DebugX;
import javafx.animation.KeyValue;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.util.Duration;

public class ScreenMainMenu extends Animation implements Screen {

	private KeyboardControls k;
	private GraphicsCore gr;
	private int ch = 0;
	
	private boolean isFocused = true;
	private boolean isInitialized = false;
	
	private AudioPlayer aud = new AudioPlayer("/audio/music/Menu.mp3");
	private AudioPlayer mmn = new AudioPlayer("/audio/sound/Menu move.mp3");
	
	@Override
	public void draw() {
		if (!isInitialized) {
			aud.setRepeat(true);
			aud.play();
			aud.setVolume(0.25);
		}
		
		start();
	}

	private boolean lastUp = false;
	private boolean lastDown = false;
	
	@Override
	public void update() {
		if (isFocused) {
			if (k.getPressed(KeyCode.LEFT)) {
				if (!lastUp && !(ch <= 0)) {
					getTimeline().playFromStart();
					getTimeline().stop();
					mmn.playFromStart();
					ch -= 1;
					getTimeline().playFromStart();
				}
				lastUp = true;
			} else {
				lastUp = false;
			}
			
			if (k.getPressed(KeyCode.RIGHT)) {
				if (!lastDown && !(ch >= 3)) {
					getTimeline().stop();
					mmn.playFromStart();
					ch += 1;
					getTimeline().playFromStart();
				}
				lastDown = true;
			} else {
				lastDown = false;
			}
			
			if (k.getPressed(KeyCode.ENTER)) {
				if (!(lastDown) && (!lastUp)) {
					if (ch == 2) {
						ScreenStarter.start(new ScreenOptions_language(this));
						getTimer().stop();
					} else if (ch == 3) {
						gr.getStage().close();
					}
				}
			}
			
			mmn.setVolume(1.0);
		}
	}

	@Override
	public void initialize(GraphicsCore g) {
		gr = g;
		k = new KeyboardControls(g);
	}

	@Override
	public void initialize() {
		if (!isInitialized) {
			DebugX.writeLine("MainMenu now starting...");
			setProperty(new SimpleDoubleProperty());
			addKeyFrame(Duration.seconds(0), new KeyValue((DoubleProperty)getProperty(), 0.25));
			addKeyFrame(Duration.seconds(1), new KeyValue((DoubleProperty)getProperty(), 0.5));
			isInitialized = true;
		}
	}

	@Override
	public void onAnimation() {
		update();

		Font f = new Font("Roboto Condensed", 36);
		
		double base = 10;
		double separ = 25;
		
		double width0 = gr.getTextWidth(f, Locale.getGlobalMessage("finite.menu.singleplayer").toUpperCase());
		double width1 = gr.getTextWidth(f, Locale.getGlobalMessage("finite.menu.multiplayer").toUpperCase());
		double width2 = gr.getTextWidth(f, Locale.getGlobalMessage("finite.menu.options").toUpperCase());
		double width3 = gr.getTextWidth(f, Locale.getGlobalMessage("finite.menu.exit").toUpperCase());
		double textH = gr.getTextHeight(f, "ABCDEFGHIJKLMNOPQRSTUWVYZ");

		double ch0 = 10;
		double ch1 = ch0 + width0 + separ;
		double ch2 = ch1 + width1 + separ;
		double ch3 = ch2 + width2 + separ;
		double hW = 50;
		double h = gr.getHeight() - textH - (hW / 2);
		
		double rectX = 0;
		double rectY = 0;
		double rectW = 0;

		gr.clear();
		
		gr.setFont(new Font("Roboto Condensed", 64));
		gr.drawText(Locale.getGlobalMessage("finite.menu.title"), Color.WHITE);

		gr.getGraphicsContext().setFill(Color.rgb(66, 66, 66));
		gr.getGraphicsContext().fillRect(0, gr.getHeight() - textH - hW, gr.getWidth(), hW + textH);
		
		if (ch == 0) {
			rectX = base + ch0;
			rectY = h;
			rectW = width0 + 10;
		} else if (ch == 1) {
			rectX = base + ch1;
			rectY = h;
			rectW = width1 + 10;
		} else if (ch == 2) {
			rectX = base + ch2;
			rectY = h;
			rectW = width2 + 10;
		} else if (ch == 3) {
			rectX = base + ch3;
			rectY = h;
			rectW = width3 + 10;
		}
		gr.getGraphicsContext().setFill(Color.GRAY.deriveColor(0, 1, 1, ((DoubleProperty)getProperty()).get()));
		gr.getGraphicsContext().fillRect(rectX - 5, rectY, rectW, textH);
		
		gr.getGraphicsContext().setStroke(Color.WHITE.deriveColor(0, 1, 1, ((DoubleProperty)getProperty()).get()));
		gr.getGraphicsContext().strokeRect(rectX - 5, rectY, rectW, textH);

		gr.setFont(f);

		gr.drawTextAtPos(Locale.getGlobalMessage("finite.menu.singleplayer").toUpperCase(), Color.WHITE, base + ch0, h);
		gr.drawTextAtPos(Locale.getGlobalMessage("finite.menu.multiplayer").toUpperCase(), Color.WHITE, base + ch1, h);
		gr.drawTextAtPos(Locale.getGlobalMessage("finite.menu.options").toUpperCase(), Color.WHITE, base + ch2, h);
		gr.drawTextAtPos(Locale.getGlobalMessage("finite.menu.exit").toUpperCase(), Color.WHITE, base + ch3, h);
		
		Font x2 = new Font("Roboto", 12);
		gr.setFont(x2);
		
		String version = Locale.getGlobalMessage("finite.version") + " (" + Locale.getGlobalMessage("finite.language") + " (" +  Locale.getGlobalMessage("finite.country")  + "))";
		gr.drawTextAtPos(version, Color.WHITE, gr.getWidth() - gr.getTextWidth(x2, version) - 10, gr.getHeight() - gr.getTextHeight(x2, version) - 10);
	}

	@Override
	public void onFinished() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onCrash() {
		aud.stop();
		mmn.stop();
		getTimer().stop();
		getTimeline().stop();
	}

}