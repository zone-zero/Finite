package finite.screens;

import composition.animation.Animation;
import composition.animation.Card;
import composition.graphics.GraphicsCore;
import composition.input.KeyboardControls;
import composition.localization.Locale;
import composition.media.AudioPlayer;
import composition.screen.Screen;
import composition.screen.ScreenStarter;
import composition.stats.DebugX;
import javafx.animation.KeyValue;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.text.Font;
import javafx.util.Duration;

public class ScreenIntroduction extends Animation implements Screen {

	private GraphicsCore gr;
	private final Card card = new Card(4);
	private KeyboardControls i;

	private AudioPlayer aud = new AudioPlayer("/audio/music/Introduction.mp3");
	
	@Override
	public void draw() {
		aud.setRepeat(true);
		aud.play();
		start();
	}

	@Override
	public void initialize(GraphicsCore g) {
		gr = g;
		i = new KeyboardControls(gr);
	}

	@Override
	public void initialize() {
		DebugX.writeLine("Introduction now starting...");
		setProperty(new SimpleDoubleProperty());
		addKeyFrame(Duration.seconds(0.5), new KeyValue((DoubleProperty)getProperty(), 0));
		addKeyFrame(Duration.seconds(2), new KeyValue((DoubleProperty)getProperty(), 1));
		addKeyFrame(Duration.seconds(3.5), new KeyValue((DoubleProperty)getProperty(), 1));
		addKeyFrame(Duration.seconds(4), new KeyValue((DoubleProperty)getProperty(), 0));
		addKeyFrame(Duration.seconds(4.25), new KeyValue((DoubleProperty)getProperty(), 0));
	}

	private Color x = Color.RED;

	@Override
	public void onAnimation() {	
		Font header = new Font("InterstatePlus-Regular", 64);
		Font body = new Font("Interstate-Light", 24);
		gr.clear();
		
		update();
		
		double o = 1.4;
		Stop[] stops = new Stop[] { new Stop(0, Color.rgb(0,0,0,0)), new Stop(o, x.deriveColor(0, 1, 1, ((DoubleProperty)getProperty()).get() - 0.605)) };
		
		if (card.getCurrent() == 0) {
			x = Color.RED;
			gr.clear(new LinearGradient(0, 0, 0, o, true, CycleMethod.NO_CYCLE, stops));
			gr.setFont(body);
			gr.drawText(Locale.getGlobalMessage("finite.composition.pref"), Color.WHITE.deriveColor(0, 0, 1, ((DoubleProperty)getProperty()).get()), 0, -74);
			gr.setFont(new Font("Liberty City Ransom", 94));
			gr.drawText(Locale.getGlobalMessage("finite.composition"), Color.WHITE.deriveColor(0, 0, 1, ((DoubleProperty)getProperty()).get()));
		} else if (card.getCurrent() == 3) {
			x = Color.rgb(0, 127, 255);
			gr.setFont(header);
			gr.clear(new LinearGradient(0, 0, 0, o, true, CycleMethod.NO_CYCLE, stops));
			gr.drawText(Locale.getGlobalMessage("finite.project.name"), Color.WHITE.deriveColor(0, 0, 1, ((DoubleProperty)getProperty()).get()), 0, -34);
			gr.setFont(body);
			gr.drawText(Locale.getGlobalMessage("finite.project.year"), Color.WHITE.deriveColor(0, 0, 1, ((DoubleProperty)getProperty()).get()), 0, 28);
		} else if (card.getCurrent() == 2) {
			x = Color.PURPLE;
			gr.setFont(header);
			gr.clear(new LinearGradient(0, 0, 0, o, true, CycleMethod.NO_CYCLE, stops));
			gr.drawText(Locale.getGlobalMessage("finite.author"), Color.WHITE.deriveColor(0, 0, 1, ((DoubleProperty)getProperty()).get()), 0, -17);
		} else if (card.getCurrent() == 1) {
			gr.setFont(header);
			gr.drawText(Locale.getGlobalMessage("finite.incomplete"), Color.WHITE.deriveColor(0, 0, 1, ((DoubleProperty)getProperty()).get()), 0, -34);
			gr.setFont(body);
			gr.drawText(Locale.getGlobalMessage("finite.incomplete.desc"), Color.WHITE.deriveColor(0, 0, 1, ((DoubleProperty)getProperty()).get()), 0, 28);
		}
	}

	@Override
	public void onFinished() {
		card.next();
		gr.clear();
		if (card.getMaximum() == card.getCurrent()) {
			aud.stop();
			stop();
			ScreenStarter.start(ScreenMainMenu.class);
			
		} else {
			play();
		}
	}

	@Override
	public void update() {
		if (i.getPressed(KeyCode.ENTER)) {
			getTimeline().stop();
			
			card.max();
			onFinished();
		}
	}

	@Override
	public void onCrash() {
		// TODO Auto-generated method stub
		
	}

}
