package finite.screens;

import composition.graphics.GraphicsCore;
import composition.localization.Locale;
import composition.screen.Screen;
import composition.stats.DebugX;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class ScreenCrash implements Screen {

	private GraphicsCore gr;
	private Stage s;
	private TextArea text;
	
	@Override
	public void draw() {
		s = gr.getStage();
		BorderPane x = new BorderPane();
		
		VBox y = new VBox();
		y.setAlignment(Pos.CENTER);
		Label top = new Label(Locale.getGlobalMessage("finite.name"));
		top.setAlignment(Pos.CENTER);
		top.setPadding(new Insets(0,0,5,0));
		top.setFont(new Font("Arial", 16));
		y.getChildren().add(top);
		
		gr.getScene().setRoot(x);
		text = new TextArea(DebugX.getLog());
		text.setFont(new Font("Courier New", 12));
		text.setEditable(false);
		
		x.setPadding(new Insets(10));
		x.setTop(y);
		x.setCenter(text);
		s.show();
	}

	@Override
	public void update() {
		
	}

	@Override
	public void initialize(GraphicsCore g) {
		gr = g;
	}

	@Override
	public void onCrash() {
		// TODO Auto-generated method stub
		
	}

}
