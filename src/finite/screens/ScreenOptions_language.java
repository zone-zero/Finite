package finite.screens;

import composition.animation.Animation;
import composition.graphics.GraphicsCore;
import composition.input.KeyboardControls;
import composition.localization.Locale;
import composition.media.AudioPlayer;
import composition.screen.Screen;
import composition.screen.ScreenStarter;
import composition.stats.DebugX;
import javafx.animation.KeyValue;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.util.Duration;

public class ScreenOptions_language extends Animation implements Screen {

	private KeyboardControls k;
	private GraphicsCore gr;
	private int ch = 0;
	private ScreenMainMenu p;

	private boolean isFocused = true;
	private boolean isInitialized = false;

	private boolean lastUp = false;
	private boolean lastDown = false;
	
	private AudioPlayer mmn = new AudioPlayer("/audio/sound/Menu move.mp3");
	
	public ScreenOptions_language(ScreenMainMenu x) {
		p = x;
	}
	
	@Override
	public void draw() {
		start();
	}
	@Override
	public void update() {
		if (isFocused && isInitialized) {
			if (k.getPressed(KeyCode.LEFT)) {
				if (!lastUp && !(ch <= 0)) {
					getTimeline().playFromStart();
					getTimeline().stop();
					mmn.playFromStart();
					ch -= 1;
					getTimeline().playFromStart();
				}
				lastUp = true;
			} else {
				lastUp = false;
			}
			
			if (k.getPressed(KeyCode.RIGHT)) {
				if (!lastDown && !(ch >= 3)) {
					getTimeline().stop();
					mmn.playFromStart();
					ch += 1;
					getTimeline().playFromStart();
				}
				lastDown = true;
			} else {
				lastDown = false;
			}
			
			if (k.getPressed(KeyCode.ENTER)) {
				if (!(lastDown) && (!lastUp)) {
					if (ch == 0) {
						Locale.start("lang/Finite", "en", "US");
					} else if (ch == 1) {
						Locale.start("lang/Finite", "fr", "FR");
					} else if (ch == 2) {
						Locale.start("lang/Finite", "it", "IT");
					} else if (ch == 3) {
						ScreenStarter.start(p);
						getTimeline().stop();
						getTimer().stop();
					}
				}
			}
			
			mmn.setVolume(1.0);
		}
	}
	@Override
	public void initialize(GraphicsCore g) {
		DebugX.writeLine("MainMenu now starting...");
		// gr = g;
		k = new KeyboardControls(g);
		setProperty(new SimpleDoubleProperty());
		addKeyFrame(Duration.seconds(0), new KeyValue((DoubleProperty)getProperty(), 0.25));
		addKeyFrame(Duration.seconds(1), new KeyValue((DoubleProperty)getProperty(), 0.5));
		isInitialized = true;
	}
	
	@Override
	public void initialize() {
		
	}
	
	@Override
	public void onAnimation() {
		try {
			if (isInitialized) {
				update();
				
				Font x = new Font("Roboto Condensed", 36);
				gr.setFont(x);
				
				double base = 10;
				double separ = 25;
				
				String ch0t = Locale.getGlobalMessage("finite.lang.en_US");
				String ch1t = Locale.getGlobalMessage("finite.lang.fr_FR");
				String ch2t = Locale.getGlobalMessage("finite.lang.it_IT");
				String ch3t = Locale.getGlobalMessage("finite.menu.back");
				
				double width0 = gr.getTextWidth(x, ch0t);
				double width1 = gr.getTextWidth(x, ch1t);
				double width2 = gr.getTextWidth(x, ch2t);
				double width3 = gr.getTextWidth(x, ch3t);

				double textH = gr.getTextHeight(x, "ABCDEFGHIJKLMNOPQRSTUWVYZ");

				double ch0 = 10;
				double ch1 = ch0 + width0 + separ;
				double ch2 = ch1 + width1 + separ;
				double ch3 = ch2 + width2 + separ;
				double hW = 50;
				double h = gr.getHeight() - textH - (hW / 2);
				
				double rectX = 0;
				double rectY = 0;
				double rectW = 0;

				gr.clear();
				gr.drawText(Locale.getGlobalMessage("finite.menu.options.languages"), Color.WHITE);

				gr.getGraphicsContext().setFill(Color.rgb(66, 66, 66));
				gr.getGraphicsContext().fillRect(0, gr.getHeight() - textH - hW, gr.getWidth(), hW + textH);
				
				if (ch == 0) {
					rectX = base + ch0;
					rectY = h;
					rectW = width0 + 10;
				} else if (ch == 1) {
					rectX = base + ch1;
					rectY = h;
					rectW = width1 + 10;
				} else if (ch == 2) {
					rectX = base + ch2;
					rectY = h;
					rectW = width2 + 10;
				} else if (ch == 3) {
					rectX = base + ch3;
					rectY = h;
					rectW = width3 + 10;
				}
				gr.getGraphicsContext().setFill(Color.GRAY.deriveColor(0, 1, 1, ((DoubleProperty)getProperty()).get()));
				gr.getGraphicsContext().fillRect(rectX - 5, rectY, rectW, textH);
				
				gr.getGraphicsContext().setStroke(Color.WHITE.deriveColor(0, 1, 1, ((DoubleProperty)getProperty()).get()));
				gr.getGraphicsContext().strokeRect(rectX - 5, rectY, rectW, textH);

				gr.drawTextAtPos(ch0t, Color.WHITE, base + ch0, h);
				gr.drawTextAtPos(ch1t, Color.WHITE, base + ch1, h);
				gr.drawTextAtPos(ch2t, Color.WHITE, base + ch2, h);
				gr.drawTextAtPos(ch3t, Color.WHITE, base + ch3, h);
				
				Font x2 = new Font("Roboto", 12);
				gr.setFont(x2);
				
				String version = Locale.getGlobalMessage("finite.version") + " (" + Locale.getGlobalMessage("finite.language") + " (" +  Locale.getGlobalMessage("finite.country")  + "))";
				gr.drawTextAtPos(version, Color.WHITE, gr.getWidth() - gr.getTextWidth(x2, version) - 10, gr.getHeight() - gr.getTextHeight(x2, version) - 10);
			}
		} catch (Exception ex) {
			DebugX.writeErr("Could not animate screen [" + this.getClass().getCanonicalName() + "]", ex);
			ScreenStarter.start(new ScreenCrash());
			
			onCrash();
			p.onCrash();
		}
	}
	@Override
	public void onFinished() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onCrash() {
		this.getTimeline().stop();
		this.getTimer().stop();
	}
	
}
