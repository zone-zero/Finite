package finite.stage;

import composition.localization.Locale;
import composition.screen.Composite;
import composition.screen.ScreenStarter;
import finite.screens.ScreenIntroduction;
import javafx.stage.Stage;

public class FiniteWindow extends Composite {

	@Override
	public void onStart(Stage s) {
		// s.setResizable(false);
		Locale.start("lang/Finite", "en", "US");
		ScreenStarter.start(ScreenIntroduction.class);
	}

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void onInit() {
		setSize(1280, 720);
	}
	
}
